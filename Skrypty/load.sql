LOAD DATA INFILE "D:/WAGAWEB/htdocs/mysql/Trans.txt"  IGNORE INTO TABLE transinfo 
FIELDS TERMINATED BY '|' 
LINES TERMINATED BY '\r\n' 
(Kurs_ID,@Data_Transakcji,Nr_Rejestracji,ID_Karty,Kod,Asortyment,Waga_Brutto,@Data_Brutto,Waga_Tara,@Data_Tara,Waga_Netto,Nr_Kwitu,Kontrahent) 
SET Data_Transakcji = str_to_date(@Data_Transakcji, '%d-%m-%Y'), 
Data_Brutto = str_to_date(@Data_Brutto, '%d-%m-%Y %H:%i'),
Data_Tara = str_to_date(@Data_Tara, '%d-%m-%Y %H:%i');