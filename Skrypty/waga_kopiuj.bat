REM Plik foldery.bat
REM SLUZY DO WYROWNYWANIA FOLDEROW Z RAPORTAMI MIEDZY Serwerem nr 2 a Serwerem nr 1

REM Kopiowanie plikow z folderu na Serwerze 2 do zmapowanego dysku z serwera 1
REM xcopy /Y "C:\Raporty\*.*" "Z:\Raporty1" /E /H

MOVE /Y "D:\Test_dysk_siec\*.*" "D:\Test_folder" 
REM MOVE /Y "D:\Waga\img\*.jpg" "D:\WAGAWEB\htdocs\www\img"

REM Wyrownanie plikow z serwera nr 1 do serwera nr 2
REM robocopy Z:\Raporty1\ C:\Raporty /E /PURGE