﻿Set-ExecutionPolicy Unrestricted -Scope CurrentUser -Force
Set-ExecutionPolicy Unrestricted -Scope Process -Force
$folder = 'D:\Test_folder' #FOLDER z ktorego jest kopiowany plik przeslany przez komputer WAGA
$filter = '*.txt' 
$destination = 'D:\WAGAWEB\htdocs\mysql'  #Folder do którego jest kopiowany i zaciagany do naszej bazy danych u nas to D:\Transinfo\
#$db = 'sqltest'
#$user = 'root'
#$pass = 'password'
#$mysql = 'C:\Program Files\MySQL\MySQL Server 5.7\bin\mysql.exe'
#$login = '-u', $user, '-p', $pass, $db

$fsw = New-Object IO.FileSystemWatcher $folder, $filter -Property @{
    IncludeSubdirectories = $true
    NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'
    }
$onCreated = Register-ObjectEvent $fsw Created -SourceIdentifier FileCreated -Action {
    $path = $Event.SourceEventArgs.Fullpath
    $name = $Event.SourceEventArgs.Name
    $changeType = $Event.SourceEventArgs.ChangeType
    $timeStamp = $Event.TimeGenerated
    Write-Host "The file '$name' was $changeType at $timeStamp"
    Move-Item $path -Destination $destination -Force -Verbose
    #ZMIANA KODOWANIA PLIKU DO UTF8 BEZ BOM
    $MyPath = 'D:\WAGAWEB\htdocs\mysql\Trans.txt'
    $MyFile = Get-Content $MyPath
    $Utf8NoBomEncoding = New-Object System.Text.UTF8Encoding($False)
    [System.IO.File]::WriteAllLines($MyPath, $MyFile, $Utf8NoBomEncoding)

    #TUTAJ MYSQL QUERRY
    Start-Process -FilePath "D:\WAGAWEB\Skrypty\LOAD.bat" -Verb runAs -Wait -PassThru;$a.ExitCode
}

$filter2 = '*.jpg'
$destination2 = 'D:\WAGAWEB\htdocs\www\img' #TUTAJ TRZEBA DAC FOLDER SKAD POBIERANE SA ZDJECIA # D:\WAGAWEB
$fsw2 = New-Object IO.FileSystemWatcher $folder, $filter2 -Property @{
    IncludeSubdirectories = $true
    NotifyFilter = [IO.NotifyFilters]'FileName, LastWrite'
    }
$onCreated2 = Register-ObjectEvent $fsw2 Created -SourceIdentifier FileCreated2 -Action {
    $path2 = $Event.SourceEventArgs.Fullpath
    $name2 = $Event.SourceEventArgs.Name
    $changeType2 = $Event.SourceEventArgs.ChangeType
    $timeStamp2 = $Event.TimeGenerated
    Write-Host "The file '$name2' was $changeType2 at $timeStamp2"
    Move-Item $path2 -Destination $destination2 -Force -Verbose
}
exit
